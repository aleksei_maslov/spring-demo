package com.example.demo.services;

import com.example.demo.dao.PartyRepository;
import com.example.demo.dao.UserRepository;
import com.example.demo.model.Party;
import com.example.demo.model.User;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TestService {
    private final UserRepository userRepository;
    private final PartyRepository partyRepository;

    public TestService(UserRepository userRepository, PartyRepository partyRepository) {
        this.userRepository = userRepository;
        this.partyRepository = partyRepository;
    }

    @Transactional
    public void test() {
        User user1 = new User();
        User user2 = new User();
        User user3 = new User();

        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);

        Party party1 = new Party();
        Party party2 = new Party();
        Party party3 = new Party();

        partyRepository.save(party1);
        partyRepository.save(party2);
        partyRepository.save(party3);

        user1.getParties().add(party1);
        user1.getParties().add(party2);
        user1.getParties().add(party3);
        user2.getParties().add(party1);
        user2.getParties().add(party2);
        user3.getParties().add(party1);

        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);

        userRepository.findByParties(party3).forEach(System.out::println);

        printAll();
    }

    @Transactional
    public void printAll() {
        userRepository.findAll().forEach(System.out::println);
        partyRepository.findAll().forEach(System.out::println);
    }
}
